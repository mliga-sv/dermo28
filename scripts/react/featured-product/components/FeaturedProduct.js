import React, { useState, useEffect } from 'react'
import AddToCartButton from 'scripts/react/product/AddToCartButton'

import { useImageSrc } from 'scripts/react/hooks/use-image-src'
import { useImageSrcSet } from 'scripts/react/hooks/use-image-src-set'

export default function FeaturedProduct({ product }) {
  // const [featured, setFeatured] = useState(null)

  const featuredImageSrcSet  = useImageSrcSet(product.imageSrcSet)
  const featuredImageSrc = useImageSrc(product.imageSrcSet, 600)

  const { translations } = window.THEME_CONFIGS

  // useEffect(() => {
  //   fetch(`/products/${handle}?view=full`, {
  //     method: 'GET',
  //   })
  //   .then(res => res.json())
  //   .then(res => {
  //     setFeatured({ ...res.product, metafields: {
  //       short_description: res.metafields.short_description
  //     }})
  //   })
  //   .catch(err => console.log(err))
  // }, [handle])
  
  return (
    <div className="featured-product__container large-container">
      <div className="section-title mb-16"><span></span></div>
      <div className="featured-product__wrapper small-container grid grid-cols-1 lg:grid-cols-2">
        {product.featured_image !== '' && <div className="featured-product__image">
            <img 
              src={featuredImageSrc} 
              srcSet={featuredImageSrcSet}
              sizes="(max-width: 1024px) 400px, 600px"
              alt="" 
              loading="lazy" 
              className="w-full" />
          </div>}
        <div className="featured-product__content mt-16 lg:mt-0 pl-0 lg:pl-8 flex flex-wrap flex-col justify-between">
          <div className="featured-product__vendor"><span>{product.vendor}</span></div>
          <div className="featured-product__title"><a href={'/products/' + product.handle} alt={product.title}>{product.title}</a></div>
          <div className="featured-product__attributes"></div>
          <div className="featured-product__description">{product.metafields.short_description}</div>
          <div className="featured-product__price-container">
            {product.compare_at_price && <div className="featured-product__original-price">{Spotview.formatMoney(product.compare_at_price)}</div>}
            <div className="featured-product__price">{Spotview.formatMoney(product.price)}</div>
          </div> 
          <div className="featured-product__addtobasket mt-16">
            <AddToCartButton quantity={1} id={product.variants[0].id} addToCartCta={translations.products.product.addToCart} available={product.available} unavailableCta={translations.products.product.unavailable} />
          </div>
        </div>
      </div>
    </div>
  )
}