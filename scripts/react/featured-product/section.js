import React from 'react'

import { getWindowVariable, safeRender } from 'scripts/react/helpers'
import FeaturedProduct from './components/FeaturedProduct'

getWindowVariable('FEATURED_PRODUCT', (product) => {
  if (product) {
    safeRender(<FeaturedProduct product={product} />, document.getElementById('featuredProduct'))
  }
})