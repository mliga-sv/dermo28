import React, { useEffect, useState } from "react";
import Accordion from "../../commons/Accordion/Accordion";

export default function AccordionWrapper({
  data,
  translations,
  themeSettings,
}) {
  const { description, accordion_sections } = data;
  const [openedAccordion, setOpenedAccordion] = useState(-1);

  useEffect(() => {
    console.log('OPENED ACCORDION', openedAccordion)
  }, [openedAccordion])
  
  return (
    <div className="large-container">
      <div className="product__details-accordion__container small-container">
        {description && (
          <Accordion
            key={-1}
            title={translations.products.product.description}
            content={description}
            opened={openedAccordion === -1}
            setOpened={() => setOpenedAccordion(-1)}
          />
        )}
        {accordion_sections.map(({ title, content, hasIcons = false }, i) => {
          return (
            <Accordion
              key={i}
              title={title}
              content={content}
              hasIcons={hasIcons}
              opened={openedAccordion === i}
              setOpened={() => setOpenedAccordion(i)}
            />
          );
        })}
      </div>
    </div>
  );
}
