import React from 'react'

import { getWindowVariable, safeRender } from 'scripts/react/helpers'
import AccordionWrapper from './components/AccordionWrapper'

getWindowVariable('ACCORDION_DETAILS', (data) => {
  safeRender(<AccordionWrapper data={data} themeSettings={window.THEME_CONFIGS.settings} translations={window.THEME_CONFIGS.translations} />, document.getElementById('accordionWrapper'))
})