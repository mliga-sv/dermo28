import { useMemo } from 'react'

/**
 * Custom hook which maps object keys and values into a string which can be used to define the srcset of an image.
 *
 * @param {object} srcSet Object with the intrinsic width of the image as keys and the image src as values.
 *
 * @returns {string} Srcset containing an image src and intrinsic width with each being seperated with a comma.
 */
function useImageSrcSet(srcSet) {
    return useMemo(() => {
        if (!srcSet) {
            return null
        }

        return Object.entries(srcSet)
            .map(([width, src]) => `${src} ${width.toLowerCase()}`)
            .join(',')
    }, [srcSet])
}

export { useImageSrcSet }
