import { useMemo } from 'react'

/**
 * Custom hook which returns image src from srset object. When JSON is parsed the object keys
 * can sometimes be converted to uppercase, therefore, both uppercase and lowercase object keys are checked.
 *
 * @param {object} srcSet Object with the intrinsic width of the image as keys and the image src as values.
 *
 * @param {number} src Desired src width which is mapped to the srcset object keys.
 *
 * @returns {string} Image src.
 */
function useImageSrc(srcSet, src) {
    return useMemo(() => {
        if (!srcSet || !src) {
            return null
        }

        return srcSet[`${src}w`] || srcSet[`${src}W`]
    }, [srcSet, src])
}

export { useImageSrc }
