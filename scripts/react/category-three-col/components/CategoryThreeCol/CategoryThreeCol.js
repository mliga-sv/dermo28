import React from 'react'

import bufalaBkg from 'assets/bufala-bck-right.png'
import SectionBackground from '../../../commons/SectionBackground'
import SectionTitle from '../../../commons/SectionTitle'

const CategoryThreeCol = ({
  id,
  cards,
  settings,
  themeConfigs
}) => {
  // const { translations: { sections: { categoryThreeCol } } } = themeConfigs
  const { sectionTitle, sectionAlign, noUppercase, enableBackground, backgroundOrientation } = settings


  return (
    <div className="mt-24 mb-24 lg:mt-24 lg:mb-24">
      <div className="category-three-col large-container">
        <SectionTitle title={sectionTitle} align={sectionAlign} noUppercase={noUppercase} />
        {enableBackground && <SectionBackground orientation={backgroundOrientation} />}
        <div className="category-three-col__cards grid mde:grid-cols-3 gap-12">
          {cards.map((card, i) => (
            <div key={i}>
              <div className="category-three-col__card">
                <div className="category-three-col__background">
                  <img src={card.image} alt={card.title} />
                </div>  
                <div className="category-three-col__details">
                  <div className="category-three-col__title" style={{ textAlign: card.titleCentered ? 'center' : 'left'}}>{card.title}</div>
                  {card.ctaLabel && card.ctaUrl && <div className="category-three-col__cta">
                    <a href={card.ctaUrl} className="button button--primary w-full">{card.ctaLabel}</a>
                  </div>}
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default CategoryThreeCol