import React from 'react'

import { getWindowVariable, safeRender } from '../helpers'
import CategoryThreeCol from './components/CategoryThreeCol'

getWindowVariable('CATEGORY_THREE_COL', (section) => {
  const { cards, id, elementId, settings } = section

  if (cards.length > 0) {
    safeRender(<CategoryThreeCol id={id} cards={cards} themeConfigs={window.THEME_CONFIGS} settings={settings} />, document.getElementById(elementId))
  }
})