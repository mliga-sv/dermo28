import React from 'react'

import { getWindowVariable, safeRender } from 'scripts/react/helpers'
import ProductDetailsWrapper from './components/ProductDetailsWrapper'

getWindowVariable('PRODUCT_DETAILS', (data) => {
  safeRender(<ProductDetailsWrapper data={data} themeSettings={window.THEME_CONFIGS.settings} translations={window.THEME_CONFIGS.translations} />, document.getElementById('productDetailsWrapper'))
})