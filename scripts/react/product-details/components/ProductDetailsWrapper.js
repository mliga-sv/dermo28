import React, { useState } from 'react'

import ProductDetailsMediaSlider from './ProductDetailsMediaSlider'
import Price from '../../product/Price'
import ProductDetailsAttributeList from './ProductDetailsAttributeList'
import Sharer from '../../product/Sharer'

import AddToCartButton from '../../product/AddToCartButton'
import RichText from '../../commons/RichText/RichText'

export default function ProductDetailsWrapper({ data, translations, themeSettings }) {
  const { variants, vendor, title, media, compare_at_price, price, metafields, tags, productSection, placeholder, available } = data
  const [quantity, setQuantity] = useState(1)

  return (
    <div className="product__details-wrapper large-container">
      <div className="product__details-container grid grid-cols-1 lg:grid-cols-2">
        <div className="product__details-media">
          <ProductDetailsMediaSlider media={media} placeholder={placeholder} />
        </div>
        <div className="product__details-info pt-12 lg:pl-12 lg:pt-0"> 
          <div className="product__details-vendor">
            <span>{vendor}</span>
          </div>
          <div className="product__details-title">
            <h1>{title}</h1>
          </div>
          {/* {metafields.prezzochilo && <div className="product__details-format">
            <p><strong>{variants[0].weight}g</strong> ({metafields.prezzochilo}€/Kg)</p>
          </div>} */}
          {metafields.formato && <div className="product__details-format">
            <p><strong>{metafields.formato}</strong></p>
          </div>}
          {metafields.short_description && <div className="product__details-short__description">
            <span>
              <p><RichText content={metafields.short_description} /></p>
            </span>
          </div>}
          <div className="product__details-price">
            <Price comparePrice={compare_at_price} price={price} />
          </div>
          <div className="product__details-controls flex flex-wrap mt-12 mb-12 items-center">
            <div className={`product__details-quantity__controls flex items-center quantity_selector_type_${productSection.quantitySelectorType} flex-100% lg:flex-none lg:mr-12`}>
              <button className="product__details-quantity__button" onClick={() => quantity > 1 ? setQuantity(quantity-1) : null}>
                <span>
                  <svg width="15px" height="4px" viewBox="0 0 15 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <g id="❤️-Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                        <g id="stepper" transform="translate(-8.000000, -17.000000)" fill="#484848">
                            <g id="Group-2" transform="translate(-0.035885, 4.000000)">
                                <rect id="Rectangle" x="8.68421053" y="13.4210526" width="13.4210526" height="3.15789474"></rect>
                            </g>
                        </g>
                    </g>
                  </svg>
                </span>
              </button>
              <input type="text" className="product__details-quantity__input" min="1" value={quantity} />
              <button className="product__details-quantity__button" onClick={() => setQuantity(quantity+1)}>
                <span>
                  <svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                      <g id="❤️-Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                          <g id="stepper" transform="translate(-85.000000, -12.000000)" fill="#484848">
                              <g id="Group" transform="translate(78.035885, 4.000000)">
                                  <path d="M16.2828947,8.68421053 L16.2827368,13.7162105 L21.3157895,13.7171053 L21.3157895,17.0723684 L16.2827368,17.0722105 L16.2828947,22.1052632 L12.9276316,22.1052632 L12.9267368,17.0722105 L7.89473684,17.0723684 L7.89473684,13.7171053 L12.9267368,13.7162105 L12.9276316,8.68421053 L16.2828947,8.68421053 Z" id="Combined-Shape"></path>
                              </g>
                          </g>
                      </g>
                  </svg>
                </span>
              </button>
            </div>
            <div className="product__details-cart__button mt-12 lg:mt-0 mb-12 lg:mb-0 flex-100% lg:flex-1 lg:mr-12 lg:text-center">
              {/* <button className="button button--primary button--big" onClick={() => addToCart()}>{translations.products.product.addToCart}</button> */}
              <AddToCartButton id={variants[0].id} quantity={quantity} addToCartCta={translations.products.product.addToCart} available={available} unavailableCta={translations.products.product.unavailable}  />
            </div>
            <div className="lg:flex-none" id="productWishlistPlaceholder"></div>
          </div>
          {metafields.attributes && metafields.attributes.length > 0 && <div className="product__details-attributes__container">
            <ProductDetailsAttributeList attributes={metafields.attributes} translations={translations} />
          </div>}
          {productSection.generalInfo && <div className="product__details-extra__info">
            <span>{productSection.generalInfo}</span>
          </div>} 
          <div className="product__details-sharer__container">
            <span className="product__details-sharer__label">{translations.products.product.share}</span>
            <Sharer settings={productSection.socialSharing} url={window.location.href} />
          </div>
        </div>
      </div>
    </div>
  )
}