import React from 'react'

export default function ProductDetailsAttributeList({ attributes, translations }) {
  return (
    <div className="product__details-attributes grid grid-cols-1 lg:grid-cols-3">
      {attributes.map(a => (
        <div className="product__details-attributes__item">
          <div className="product__details-attributes__item__image">
            <img src={a.icon} alt="" className="no-radius" />
          </div>
          <div className="product__details-attributes__item__label">
            <span>{a.label}</span>
          </div>
        </div>
      ))}
    </div>
  )
}