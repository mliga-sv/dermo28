import React from 'react'

import { ButtonBack, ButtonNext, CarouselProvider, DotGroup, Slider, Slide } from 'pure-react-carousel'

import { useImageSrc } from 'scripts/react/hooks/use-image-src'
import { useImageSrcSet } from 'scripts/react/hooks/use-image-src-set'
import parse from 'html-react-parser'
import carouselArrow from 'assets/carousel-arrow.png'

export default function ProductDetailsMediaSlider ({ media, placeholder }) {
  return (
    media && media.length > 0 ? <CarouselProvider
    className="product__details-media__slider"
    naturalSlideWidth={100}
    naturalSlideHeight={100}
    totalSlides={media.length}
    dragEnabled={true}
    isIntrinsicHeight={true}
    infinite
    isPlaying
    interval={5000}
    >
      <div className="product__details-media__slider__controls">
        <DotGroup />
      </div>
      <Slider>
        {media.map(m => (
          <Slide>
            <img src={m.src} alt="" className="product__details-media__slider__image" />
          </Slide>
        ))}
      </Slider>
      <ButtonBack><img src={carouselArrow} alt="Back" /></ButtonBack>
      <ButtonNext><img src={carouselArrow} alt="Next" /></ButtonNext>
    </CarouselProvider>
    : parse(placeholder)
  )
}