import React from 'react'

export default function Price({ comparePrice, price }) {
  return (
    <div className="product__price-container">
      {comparePrice && <span className="product__compare-price">
          {Spotview.formatMoney((comparePrice / 100).toFixed(2))}
        </span>}
      {price && <span className="product__price">
          {Spotview.formatMoney((price / 100).toFixed(2))}
        </span>}
    </div>
  )
}