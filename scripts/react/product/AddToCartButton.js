import React from 'react'
import cartIcon from 'assets/cart-button-card.png'
import store from 'scripts/state'
import { useWindowWidth } from '@react-hook/window-size'

import notAvailable from 'assets/not-available.png'

export default function AddToCartButton({ quantity, id, isProductCard, addToCartCta, available, unavailableCta, addToCartButtonType }) {
  const changeCart = store(state => state.changeCart)
  const toggleMinicart = store(state => state.toggleMinicart)
  const minicart = store(state => state.minicart)

  const windowWidth = useWindowWidth()
  const themeSettings = window.THEME_CONFIGS.settings
  const isMobile = themeSettings.breakpoints.lg && windowWidth < themeSettings.breakpoints.lg

  const addToCart = (e) => {
    e.preventDefault()

    return fetch('/cart/add.js', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        items: [
          {
            quantity,
            id: id
          }
        ]
      })
    })
    .then(res => {
      changeCart()
      if (!minicart && !isMobile) {
        toggleMinicart()
      }
      else if (isMobile) {
        const addToCartNotify = document.querySelector('#addToCartNotify')
        addToCartNotify.classList.add('opened')
        setTimeout(() => {
          addToCartNotify.classList.remove('opened')
        }, 1500)
      }
      else {
      }
    })
    .catch(err => console.log('ERROR', err))
  }

  return (
    available ? (
      !isProductCard || addToCartButtonType === 'button' ?
        <button className="button button--primary w-full" onClick={(e) => addToCart(e)}>{addToCartCta}</button> :
        <button className="button button--primary button--cart--icon"  onClick={(e) => addToCart(e)}>
          <img src={cartIcon} alt="Add to Cart" />
        </button>
    ) : (
      !isProductCard || addToCartButtonType === 'button' ?
        <button className="button button--secondary w-full" disabled>{unavailableCta}</button> :
        <button className="button button--secondary button--notavailable--icon">
          <img src={notAvailable} alt="Not available" />
        </button>
    )
  )
}