import React from 'react';
import {
  FacebookShareButton,
  EmailShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from 'react-share';
import {
  EmailIcon,
  FacebookIcon,
  TwitterIcon,
  WhatsappIcon,
} from 'react-share';

export default function Sharer({
  settings: {
    facebook,
    facebookIcon,
    whatsapp,
    whatsappIcon,
    email,
    emailIcon,
    twitter,
    twitterIcon,
  },
  url,
}) {
  return (
    <div className='product__details-sharer flex justify-center'>
      {facebook && (
        <FacebookShareButton url={url}>
          {facebookIcon ? (
            <img
              src={facebookIcon}
              alt='Facebook Icon'
              className='max-w-31px mx-3'
            />
          ) : (
            <FacebookIcon size={32} round={true} />
          )}
        </FacebookShareButton>
      )}
      {whatsapp && (
        <WhatsappShareButton url={url}>
          {whatsappIcon ? (
            <img
              src={whatsappIcon}
              alt='Whatsapp Icon'
              className='max-w-31px mx-3'
            />
          ) : (
            <WhatsappIcon size={32} round={true} />
          )}
        </WhatsappShareButton>
      )}
      {email && (
        <EmailShareButton url={url}>
          {emailIcon ? (
            <img src={emailIcon} alt='Email Icon' className='max-w-31px mx-3' />
          ) : (
            <EmailIcon size={32} round={true} />
          )}
        </EmailShareButton>
      )}
      {twitter && (
        <TwitterShareButton url={url}>
          {twitterIcon ? (
            <img
              src={twitterIcon}
              alt='Twitter Icon'
              className='max-w-31px mx-3'
            />
          ) : (
            <TwitterIcon size={32} round={true} />
          )}
        </TwitterShareButton>
      )}
    </div>
  );
}
