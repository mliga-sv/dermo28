import React from 'react'
import parse from 'html-react-parser'

export default function RichText({ content }) {
  return <div className="rich-text">
    {parse(content)}
  </div>
}
