import React from 'react'

import { useWindowWidth } from '@react-hook/window-size'

const SectionBackground = ({ orientation }) => {
  const windowWidth = useWindowWidth()
  
  
  const { settings: { breakpoints, backgrounds } } = window.THEME_CONFIGS
  
  const selectedImage = orientation === 'left' ? backgrounds.black_image_left : backgrounds.black_image_right;

  const isMobile = breakpoints.mde && windowWidth < breakpoints.mde

  return (
    !isMobile && <div className="section-background">
      <img src={selectedImage} alt="" className="no-radius" />
    </div>
  )
}

export default SectionBackground