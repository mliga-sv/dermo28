import React from 'react'

const SectionTitle = ({ title, align, noUppercase }) => {
  return (
    <div className={`section-title mb-8 ${align === 'center' ? 'text-center' : (align === 'right' ? 'text-right' : '')} ${noUppercase ? 'section-title-nouppercase' : ''}`}>
      <span>{title}</span>
    </div>
  )
}

export default SectionTitle