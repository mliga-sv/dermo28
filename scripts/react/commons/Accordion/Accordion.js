import React, { useState } from "react";
import arrow from "assets/carousel-arrow.png";
import RichText from "../RichText/RichText";
import classNames from "classnames";

export default function Accordion({
  title,
  content,
  children,
  hasIcons = false,
  opened,
  setOpened,
}) {
  return (
    <div className={"accordion " + (opened ? "opened" : "")}>
      <div className="accordion-title" onClick={setOpened}>
        {title}
        <span className="arrow">
          {opened ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-11 w-11"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M5 15l7-7 7 7"
              />
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-11 w-11"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M19 9l-7 7-7-7"
              />
            </svg>
          )}
        </span>
      </div>
      {opened &&
        (content ? (
          <div
            className={classNames("accordion-content", {
              "grid grid-cols-2 lg:grid-cols-4 gap-x-10": hasIcons,
            })}
          >
            {hasIcons ? (
              <>
                {content.map((item, i) => (
                  <div key={i} className="flex flex-col gap-y-8 items-center">
                    <img src={item.icon} alt={item.label} />
                    <span className="font-bold">{item.label}</span>
                  </div>
                ))}
              </>
            ) : (
              <RichText content={content} />
            )}
          </div>
        ) : (
          <div className="accordion-content">{children}</div>
        ))}
    </div>
  );
}
