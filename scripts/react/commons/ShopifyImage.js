import React from 'react'

import { useImageSrc } from 'scripts/react/hooks/use-image-src'
import { useImageSrcSet } from 'scripts/react/hooks/use-image-src-set'

export default function ShopifyImage({ image }) {

  const srcSet = {}

  if (image.width >= 288) {
    srcSet['288w'] = 
  }
    image | img_url: '288x' }} 288w
  if (image.width >= 576) {

  }
  image | img_url: '576x' }} 576w
  if (image.width >= 750) {

  }
  image | img_url: '750x' }} 750w
  if (image.width >= 1100) {

  }
  image | img_url: '1100x' }} 1100w
  if (image.width >= 1500) {

  }
  media.preview_image | img_url: '1500x' }} 1500w

  return <img
    srcset={useImageSrcSet(srcSet)}
  />
}