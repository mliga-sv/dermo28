import React, { useState, useEffect } from 'react'

import { useWindowWidth } from '@react-hook/window-size'

import HeaderIcon from '../HeaderIcon'

import store from 'scripts/state'

const Header = ({ customer, settings: { 
  cartButtonVisibility,
  cartButtonImage,
  cartButton,
  headerLayoutType,
  languageSelectorVisibility,
  navigationButtonVisibility,
  searchButtonVisibility,
  searchButtonImage,
  searchButton,
  userProfileButtonVisibility,
  userProfileButtonImage,
  userProfileButton,
  logoImageUrl,
  shopTitle
 }, themeSettings, languages }) => {
  const shrink = store(state => state.shrink)
  const toggle = store(state => state.toggle)
  const cartLength = store(state => state.cartLength)

  const toggleMinicart = store(state => state.toggleMinicart)

  const isHeaderShrinked = store(state => state.header)

  const windowWidth = useWindowWidth()
  const [scrollTop, setScrollTop] = useState(0)

  const isMobile = themeSettings.breakpoints.lg && windowWidth < themeSettings.breakpoints.lg

  const headerElement = document.getElementsByClassName('header__content') && document.getElementsByClassName('header__content')[0]
  const announcementBarElement = document.getElementsByClassName('announcement-bar') && document.getElementsByClassName('announcement-bar')[0]

  const searchBtn = {
    type: 'search',
    image: searchButtonImage,
    ...searchButton
  }

  const userProfileBtn = {
    type: 'person',
    image: userProfileButtonImage,
    ...userProfileButton
  }

  const cartBtn = {
    type: 'cart',
    image: cartButtonImage,
    ...cartButton
  }

  useEffect(() => {
    const onScroll = e => {
      setScrollTop(document.documentElement.scrollTop);
    };
    window.addEventListener("scroll", onScroll);

    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  useEffect(() => {
    if (!isMobile) {
      if (scrollTop > 40) {
        shrink(true)
      }
      else {
        shrink(false)
      }
    }
  }, [scrollTop])

  useEffect(() => {
    if (isHeaderShrinked) {
      headerElement && headerElement.classList.add('shrinked')
      announcementBarElement && announcementBarElement.classList.add('shrinked')
    }
    else {
      headerElement && headerElement.classList.remove('shrinked')
      announcementBarElement && announcementBarElement.classList.remove('shrinked')
    }
  }, [isHeaderShrinked])

  const renderHeaderLayout = () => {
    switch (headerLayoutType) {
      case '3-columns-with-centered-logo':
      default:
        return showThreeColWithCenteredLogo()
    }
  }

  const showThreeColWithCenteredLogo = () => {
    const leftCol = []
    const rightCol = []

    if (navigationButtonVisibility === 'left') {
      leftCol.push({
        type: 'navigation'
      })
    }
    else if (navigationButtonVisibility === 'right') {
      rightCol.push({
        type: 'navigation'
      })
    }
    else {
      // INFO: se hidden, per precisa scelta, su mobile sarà comunque sempre a sinistra
      if (isMobile) {
        leftCol.push({
          type: 'navigation'
        })
      }
    }

    // INFO: per precisa scelta, il pulsante search viene nascosto da mobile mostrando la ricerca espansa
    if (!isMobile) {
      if (searchButtonVisibility === 'left') {
        leftCol.push(searchBtn)
      }
      else if (searchButtonVisibility === 'right') {
        rightCol.push(searchBtn)
      }
    }

    // INFO: per precisa scelta, il selettore della lingua viene sempre posto a sinistra su mobile
    if (!isMobile) {
      if (languageSelectorVisibility === 'left') {
        leftCol.push({
          type: 'language'
        })
      }
      else if (languageSelectorVisibility === 'right') {
        rightCol.push({
          type: 'language'
        })
      }
    }
    else {
      if (languageSelectorVisibility !== 'hidden') {
        leftCol.push({
          type: 'language'
        })
      }
    }

    if (userProfileButtonVisibility === 'left') {
      leftCol.push(userProfileBtn)
    }
    else if (userProfileButtonVisibility === 'right') {
      rightCol.push(userProfileBtn)
    }

    if (cartButtonVisibility === 'left') {
      leftCol.push(cartBtn)
    }
    else if (cartButtonVisibility === 'right') {
      rightCol.push(cartBtn)
    }

    const renderLeftCol = () => {
      return (
        <>
          {leftCol.map((i, idx) => <HeaderIcon key={'left_' + idx} icon={i} menuDropdownAction={toggle} languages={languages} />)}
        </>
      )
    }

    const renderRightCol = () => {
      return (
        <>
          {rightCol.map((i, idx) => <HeaderIcon key={'right_' + idx} icon={i} languages={languages} toggleMinicart={toggleMinicart} isMobile={isMobile} cartLength={cartLength} customer={customer} />)}
        </>
      )
    }

    return (
      <div className="header__three-col-w-centered-logo flex h-full"> 
        <div className="header__left-col flex flex-row items-center w-1/4 justify-start">{renderLeftCol()}</div>
        <div className="flex flex-auto items-center header__center-col">
          <a href="/" className="mx-auto header__logo" aria-label={shopTitle}>
            <img className="no-radius" src={logoImageUrl} alt="" width="225" height="46" />
          </a>
        </div>
        <div className="header__right-col flex flex-row items-center w-1/4 justify-end">{renderRightCol()}</div>
      </div>
    )
  }

  return (
    <div className="header__content large-container">
      {renderHeaderLayout()}
    </div>
  )
}

export default Header