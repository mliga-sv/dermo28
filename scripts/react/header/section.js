import React from 'react'

import { getWindowVariable, safeRender } from "../helpers";

import Header from "./components/Header/index";

getWindowVariable('HEADER_CONFIGS', (config) => {
  safeRender(<Header customer={config.customer} settings={config.settings} languages={config.languages} themeSettings={window.THEME_CONFIGS.settings} />, document.getElementById("shopify-section-header"))
})