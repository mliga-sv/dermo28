import { render } from "react-dom"
import has from "has"

function getWindowVariable(key, callback = null) {
  if (!has(window, key)) {
      return undefined
  }

  return callback ? callback(window[key]) : window[key]
}

function safeRender(component, root) {
  if (!root) {
    return
  }

  try {
    render(component, root)
  }
  catch {
    console.error("Failed to render a component")
  }
}

export { 
  safeRender,
  getWindowVariable
}