import React from 'react'

import { getWindowVariable, safeRender } from 'scripts/react/helpers'
import AddToCartButton from '../product/AddToCartButton'

getWindowVariable('CURRENT_PRODUCTS', (data) => {
  const addToCartCta = window.THEME_CONFIGS.translations && window.THEME_CONFIGS.translations.products && window.THEME_CONFIGS.translations.products.product.addToCart
  const unavailableCta = window.THEME_CONFIGS.translations && window.THEME_CONFIGS.translations.products && window.THEME_CONFIGS.translations.products.product.unavailable
  
  Object.keys(data).map(k => {
    safeRender(<AddToCartButton quantity={1} id={k} addToCartCta={addToCartCta} isProductCard addToCartButtonType={data[k].addToCartButtonType} available={data[k].available} unavailableCta={unavailableCta} />, document.getElementById(`addToCart_Product_${k}`))
  })
})