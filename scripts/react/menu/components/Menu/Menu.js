import React, { useState, useEffect } from "react";

import store from "scripts/state";

import { useWindowWidth } from "@react-hook/window-size";

import closeIcon from "../../../../../assets/close.png";
import menuArrow from "../../../../../assets/arrow-menu.png";

import facebookIcon from "../../../../../assets/facebook.png";
import instagramIcon from "../../../../../assets/instagram.png";
import youtubeIcon from "../../../../../assets/youtube.png";

import bufalaBkg from "../../../../../assets/bufala-bck-right.png";

const Menu = ({
  type,
  blocks,
  socials,
  themeSettings,
  translations,
  enabledSocials,
  backgroundImage,
}) => {
  const isMenuOpen = store((state) => state.menu);
  const isHeaderShrinked = store((state) => state.header);

  const toggle = store((state) => state.toggle);

  const windowWidth = useWindowWidth();
  const isMobile =
    themeSettings.breakpoints.lg && windowWidth < themeSettings.breakpoints.lg;

  const menuElement =
    document.getElementsByClassName("menu__wrapper") &&
    document.getElementsByClassName("menu__wrapper")[0];

  useEffect(() => {
    if (isMenuOpen) {
      menuElement && menuElement.classList.add("opened");
      document.body.style.overflow = "hidden";
    } else {
      menuElement && menuElement.classList.remove("opened");
      document.body.style.overflow = "auto";
    }
  }, [isMenuOpen]);

  const secondLevelMenu = blocks.reduce((acc, b, i) => {
    if (b.children && b.children.length > 0) {
      const hash = b.title.toLowerCase().replaceAll(" ", "_");
      acc[`secondLevel${i}_${hash}`] = {
        parentTitle: b.title,
        children: b.children,
      };
    }

    return acc;
  }, {});

  const openLevel = (className) => {
    const el =
      document.getElementsByClassName(className) &&
      document.getElementsByClassName(className)[0];

    if (el) {
      el.classList.add("opened");
    }
  };

  const closeLevel = (className) => {
    const el =
      document.getElementsByClassName(className) &&
      document.getElementsByClassName(className)[0];
    if (el) {
      el.classList.remove("opened");
    }
  };

  const toggleLevel = (className) => {
    const el =
      document.getElementsByClassName(className) &&
      document.getElementsByClassName(className)[0];
    const parent = el.closest(".menu__item-has-third-level");

    if (el.classList.contains("opened")) {
      closeLevel(className);
      parent.classList.remove("opened");
    } else {
      openLevel(className);
      parent.classList.add("opened");
    }
  };

  return (
    <div
      className={`menu__wrapper ${
        type === "horizontal" && !isMobile
          ? "horizontal-menu__wrapper"
          : "hamburger-menu__wrapper"
      } ${isMenuOpen ? "opened" : ""} ${
        enabledSocials ? "has-socials-enabled" : ""
      }`}
    >
      <div className={`menu__content ${isHeaderShrinked ? "shrinked" : ""}`}>
        <img
          src={closeIcon}
          alt=""
          className="menu__close-icon"
          onClick={toggle}
        />
        <ul className="menu__level-container menu___first-level-container">
          {blocks.map((b, i) => {
            if (b.children && b.children.length > 0) {
              const secondLevelKey = `secondLevel${i}_${b.title
                .toLowerCase()
                .replaceAll(" ", "_")}`;

              return type === "hamburger" || isMobile ? (
                <li key={i} onClick={() => openLevel(secondLevelKey)}>
                  {i > 0 ? <a href={b.url}>{b.title}</a> : b.title}
                  <span className="menu__item-arrow">
                    <img src={menuArrow} alt="" />
                  </span>
                </li>
              ) : (
                <li
                  key={i}
                  className=""
                  onMouseEnter={(e) => openLevel(secondLevelKey)}
                  onMouseLeave={(e) => closeLevel(secondLevelKey)}
                >
                  <div className="menu__item-title">
                    {b.title}
                    <span className="menu__item-arrow">
                      <img src={menuArrow} alt="" />
                    </span>
                  </div>
                  {secondLevelMenu[secondLevelKey].children &&
                    secondLevelMenu[secondLevelKey].children.length && (
                      <div
                        className={`menu__level-container menu__second-level-container ${secondLevelKey}`}
                      >
                        <div className="large-container grid grid-cols-5 gap-6">
                          {secondLevelMenu[secondLevelKey].children.map(
                            (b, ib) => (
                              <div key={ib} className="pr-6">
                                {b.url && b.url !== "#" ? (
                                  <span className="menu__second-level-title">
                                    <a href={b.url}>{b.title}</a>
                                  </span>
                                ) : (
                                  <span className="menu__second-level-title">
                                    {b.title}
                                  </span>
                                )}
                                {b.children && b.children.length && (
                                  <ul className="menu__level-container menu__third-level-container">
                                    {b.children.map((sb, sib) => (
                                      <li key={sib}>
                                        <a href={sb.url}>{sb.title}</a>
                                      </li>
                                    ))}
                                  </ul>
                                )}
                              </div>
                            )
                          )}
                        </div>
                      </div>
                    )}
                </li>
              );
            }

            return (
              <li key={i}>
                <a href={b.url}>{b.title}</a>
              </li>
            );
          })}
        </ul>
        {(type === "hamburger" || isMobile) &&
          Object.keys(secondLevelMenu).map((k, i) => (
            <ul
              key={k}
              className={`menu__level-container menu__second-level-container ${k}`}
            >
              <li onClick={() => closeLevel(k)}>
                <span className="menu__item-heading">
                  {secondLevelMenu[k].parentTitle}
                </span>

                <span className="menu__item-arrow">
                  <img src={menuArrow} alt="" />
                </span>
              </li>
              {secondLevelMenu[k].children.map((b, ib) => {
                const thirdLevelKey = `thirdLevel${i}_${b.title
                  .toLowerCase()
                  .replaceAll(" ", "_")}`;

                return (
                  <li
                    key={ib}
                    className={`${
                      b.children && b.children.length
                        ? "menu__item-has-third-level"
                        : ""
                    }`}
                    onClick={() =>
                      b.children &&
                      b.children.length &&
                      toggleLevel(thirdLevelKey)
                    }
                  >
                    <span>
                      <a href={b.url}>{b.title}</a>

                      {b.children && b.children.length && (
                        <span className="menu__item-arrow">
                          <img src={menuArrow} alt="" />
                        </span>
                      )}
                    </span>

                    {b.children && b.children.length && (
                      <ul
                        className={`menu__level-container menu__third-level-container ${thirdLevelKey}`}
                      >
                        {b.children.map((sb, sib) => (
                          <li key={sib}>
                            <a href={sb.url}>{sb.title}</a>
                          </li>
                        ))}
                      </ul>
                    )}
                  </li>
                );
              })}
            </ul>
          ))}
        {enabledSocials && Object.keys(socials).length > 0 && (
          <div className="menu__social-section">
            <p className="heading font-body uppercase font-semibold">
              {translations.sections.menu.socialHeading}
            </p>
            <div className="menu__social-items">
              {socials.facebook && (
                <a
                  className="menu__social-item menu__social-item-facebook"
                  href={socials.facebook}
                  aria-label="Facebook"
                >
                  <img src={facebookIcon} alt="" />
                </a>
              )}
              {socials.instagram && (
                <a
                  className="menu__social-item menu__social-item-instagram"
                  href={socials.instagram}
                  aria-label="Instagram"
                >
                  <img src={instagramIcon} alt="" />
                </a>
              )}
              {socials.youtube && (
                <a
                  className="menu__social-item menu__social-item-youtube"
                  href={socials.youtube}
                  aria-label="Youtube"
                >
                  <img src={youtubeIcon} alt="" />
                </a>
              )}
            </div>
          </div>
        )}
        {backgroundImage && (
          <div className="menu__bkg">
            <img src={backgroundImage} alt="" className="no-radius" />
          </div>
        )}
      </div>
    </div>
  );
};

export default Menu;
