import React from 'react'

import Menu from './components/Menu/index'

import { getWindowVariable, safeRender } from '../helpers'

getWindowVariable('MENU_CONFIGS', (config) => {
  safeRender(<Menu type={config.type} enabledSocials={config.enabledSocials} blocks={config.blocks} socials={config.socials} themeSettings={window.THEME_CONFIGS.settings} translations={window.THEME_CONFIGS.translations} backgroundImage={config.backgroundImage} />, document.getElementById('shopify-section-menu'))
})