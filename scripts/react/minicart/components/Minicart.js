import React, { useState, useRef, useMemo, useEffect } from "react";

import store from "scripts/state";
import Price from "../../product/Price";
import binIcon from "assets/bin.png";
import { getState } from "../../../state";

export default function Minicart({
  translations,
  themeSettings: { freeShippingThreshold },
  minicartSettings: { quantitySelectorType },
}) {
  const [cart, setCart] = useState({});
  const {
    sections: {
      cart: { goToCart, checkout, subtotal, shippingCost },
    },
  } = translations;
  const ref = useRef(null);
  const { subscribe } = store;
  const changeCart = store((state) => state.changeCart);

  subscribe(
    (state) => state.cart,
    (state) => {
      setCart(state);
    }
  );

  const toggleMinicart = store((state) => state.toggleMinicart);

  const cartItems = useMemo(() => cart.items, [cart]);

  const updateQuantity = (e, id, action) => {
    const input = e.target
      .closest(".cart__item")
      .querySelector(".minicart-quantity__input");
    let value = parseInt(input.value);

    if (action === "subtract") {
      if (value > 0) {
        value = value - 1;

        fetch("/cart/update.js", {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            updates: {
              [id]: value,
            },
          }),
        })
          .then((res) => {
            changeCart();
            input.value = value;
          })
          .catch((err) => console.log(err));
      }
    } else if (action === "delete") {
      fetch("/cart/update.js", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          updates: {
            [id]: 0,
          },
        }),
      })
        .then((res) => {
          changeCart();
        })
        .catch((err) => console.log(err));
    } else {
      value = value + 1;

      fetch("/cart/add.js", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          items: [
            {
              value,
              id,
            },
          ],
        }),
      })
        .then((res) => {
          changeCart();
          input.value = value;
        })
        .catch((err) => console.log(err));
    }
  };

  const renderSpedizioneGratuita = (threshold) => {
    if (cart.total_price < threshold) {
      const remainingPriceToFreeShipping = Spotview.formatMoney(
        threshold - cart.total_price
      );
      return `Sei a ${remainingPriceToFreeShipping} dalla spedizione gratuita`;
    }

    return `Spedizione gratuita`;
  };

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (ref.current && !ref.current.contains(event.target)) {
        if (getState().minicart) toggleMinicart();
      }
    };
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  }, []);

  return (
    <div ref={ref} className="ajax-cart-drawer">
      <div className="drawer-header" style={{ position: "relative" }}>
        <div
          className="ajax-cart-drawer__close js-ajax-cart-drawer-close"
          onClick={toggleMinicart}
        >
          &#10005;
        </div>
        <h2 className="ajax-cart-drawer__header__title">
          {translations.templates.cart.cart}
        </h2>
      </div>
      {freeShippingThreshold && (
        <div className="drawer-shipping negative-section">
          <span id="kdo--freeshipping" style={{ alignSelf: "center" }}>
            {renderSpedizioneGratuita(
              parseFloat(freeShippingThreshold).toFixed(2) * 100
            )}
          </span>
          <svg
            version="1.1"
            id="Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            x="0px"
            y="0px"
            viewBox="0 0 26 24"
            style={{ enableBackground: "new 0 0 26 24" }}
            xmlSpace="preserve"
            className="icon-free-shipping"
          >
            <g id="Symbols">
              <g id="pre-header" transform="translate(-871.000000, -6.000000)">
                <g
                  id="free-shipping-white"
                  transform="translate(871.000000, 6.000000)"
                >
                  <path
                    id="Shipping"
                    className="st0"
                    d="M14.7,0c1.5,0,3,0.7,3.9,1.8L18.8,2l5.6,7.2c0.4,0.5,0.6,1.1,0.7,1.7l0,0.2v6.6
                  c0,1.7-1.3,3-2.9,3.1l-0.2,0l-1.5,0c-0.8,1.8-2.6,3.1-4.8,3.1s-4-1.3-4.8-3.1l-3.6,0v-2.1h3.1c0-2.9,2.3-5.2,5.2-5.2
                  c2.8,0,5.1,2.2,5.2,5l0,0.2h1c0.5,0,1-0.4,1-0.9l0-0.1v-6.6c0-0.2,0-0.4-0.1-0.5l-0.1-0.1l-5.6-7.2c-0.6-0.7-1.4-1.1-2.3-1.2
                  l-0.2,0H3.1V0H14.7z M15.7,15.7c-1.7,0-3.1,1.4-3.1,3.1s1.4,3.1,3.1,3.1s3.1-1.4,3.1-3.1S17.4,15.7,15.7,15.7z M9.4,12.5v2.1H3.1
                  v-2.1H9.4z M6.3,6.3v2.1H0V6.3H6.3z"
                  />
                </g>
              </g>
            </g>
          </svg>
        </div>
      )}
      <div
        className="ajax-cart-drawer__content js-ajax-cart-drawer-content"
        style={{
          padding: "10px 30px",
          maxHeight: "calc(100vh - 360px)",
          overflowX: "scroll",
        }}
      >
        <div className="kdo--content">
          {cartItems &&
            cartItems.map((item, i) => (
              <div key={i} className="row cart__item flex flex-wrap mt-12">
                <div className="cart__item-image flex-shrink-0 flex-1/2">
                  <a className="cart__item-image-link" href={item.url}>
                    {/* <ShopifyImage src={item.image} size="compact" alt={item.alt} /> */}
                    <span className="visually-hidden">{item.title}</span>
                    <img
                      src={item.image}
                      alt={item.title}
                      loading="lazy"
                      width="110"
                      height="110"
                    />
                  </a>
                </div>
                <div className="cart__item-details flex-grow flex-1/2">
                  <div className="cart__item-title-container">
                    <div>
                      <div className="cart__item-vendor">{item.vendor}</div>
                      <a
                        href={item.url}
                        className="cart__item-title"
                        style={{
                          color: "#01540a !important",
                          textTransform: "uppercase !important",
                          fontWeight: "bold !important",
                        }}
                      >
                        {item.title}
                      </a>
                    </div>
                  </div>
                  <div className="cart__item-price_container">
                    {/* {item.line_level_discount_allocations.length > 0 && <div className="cart__discount-allocations">
                    {item.line_level_discount_allocations.map((lineItemDiscount, i) => (
                      <>
                        <span style={{ display: 'block', whiteSpace: 'nowrap',textTransform: 'uppercase', fontWeight: 'bold', fontSize: '12px', color: lineItemDiscount.discount_application.title === 'CartaClub' ? '#97be0d' : '#b90172'}}>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" style={{ width: '14px', fill: lineItemDiscount.discount_application.title === 'CartaClub' ? '#97be0d' : '#b90172'}}>
                          <path d="M17.78 3.09C17.45 2.443 16.778 2 16 2h-5.165c-.535 0-1.046.214-1.422.593l-6.82 6.89c0 .002 0 .003-.002.003-.245.253-.413.554-.5.874L.738 8.055c-.56-.953-.24-2.178.712-2.737L9.823.425C10.284.155 10.834.08 11.35.22l4.99 1.337c.755.203 1.293.814 1.44 1.533z" fillOpacity=".55"></path>
                          <path d="M10.835 2H16c1.105 0 2 .895 2 2v5.172c0 .53-.21 1.04-.586 1.414l-6.818 6.818c-.777.778-2.036.782-2.82.01l-5.166-5.1c-.786-.775-.794-2.04-.02-2.828.002 0 .003 0 .003-.002l6.82-6.89C9.79 2.214 10.3 2 10.835 2zM13.5 8c.828 0 1.5-.672 1.5-1.5S14.328 5 13.5 5 12 5.672 12 6.5 12.672 8 13.5 8z"></path>
                        </svg>
                        
                        {lineItemDiscount.discount_application.title} 
                        </span>
                      </>
                    ))}
                  </div>} */}

                    <div className="container flex">
                      <div className="left flex-grow">
                        {/* {item.final_line_price < item.original_line_price && (
                        <div className="cart__item__discount-container">
                          <div className="cart__item-discount_percentage">
                            <strong>-{( Math.round( ( ( (item.original_line_price - item.final_line_price) *100.0) / item.original_line_price ))) + '%'}</strong>
                          </div>
                          <span className="cart__item-variant-price-was">
                            <strike>{Spotview.formatMoney(item.original_line_price)}</strike>
                          </span>
                        </div>
                      )} */}
                        <Price
                          comparePrice={
                            item.product && item.product.compare_at_price
                          }
                          price={item.product && item.product.price}
                        />

                        <div
                          className={`minicart-quantity__controls quantity_selector_type_${quantitySelectorType}`}
                        >
                          <button
                            className="minicart-quantity__button"
                            onClick={(e) =>
                              updateQuantity(e, item.variant_id, "subtract")
                            }
                          >
                            <span>
                              <svg
                                width="15px"
                                height="4px"
                                viewBox="0 0 15 4"
                                version="1.1"
                                xmlns="http://www.w3.org/2000/svg"
                                xmlnsXlink="http://www.w3.org/1999/xlink"
                              >
                                <g
                                  id="❤️-Symbols"
                                  stroke="none"
                                  strokeWidth="1"
                                  fill="none"
                                  fillRule="evenodd"
                                >
                                  <g
                                    id="stepper"
                                    transform="translate(-8.000000, -17.000000)"
                                    fill="#484848"
                                  >
                                    <g
                                      id="Group-2"
                                      transform="translate(-0.035885, 4.000000)"
                                    >
                                      <rect
                                        id="Rectangle"
                                        x="8.68421053"
                                        y="13.4210526"
                                        width="13.4210526"
                                        height="3.15789474"
                                      ></rect>
                                    </g>
                                  </g>
                                </g>
                              </svg>
                            </span>
                          </button>
                          <input
                            type="text"
                            className="minicart-quantity__input"
                            min="1"
                            value={item.quantity}
                            aria-label="Quantity"
                          />
                          <button
                            className="minicart-quantity__button"
                            onClick={(e) =>
                              updateQuantity(e, item.variant_id, "add")
                            }
                          >
                            <span>
                              <svg
                                width="15px"
                                height="15px"
                                viewBox="0 0 15 15"
                                version="1.1"
                                xmlns="http://www.w3.org/2000/svg"
                                xmlnsXlink="http://www.w3.org/1999/xlink"
                              >
                                <g
                                  id="❤️-Symbols"
                                  stroke="none"
                                  strokeWidth="1"
                                  fill="none"
                                  fillRule="evenodd"
                                >
                                  <g
                                    id="stepper"
                                    transform="translate(-85.000000, -12.000000)"
                                    fill="#484848"
                                  >
                                    <g
                                      id="Group"
                                      transform="translate(78.035885, 4.000000)"
                                    >
                                      <path
                                        d="M16.2828947,8.68421053 L16.2827368,13.7162105 L21.3157895,13.7171053 L21.3157895,17.0723684 L16.2827368,17.0722105 L16.2828947,22.1052632 L12.9276316,22.1052632 L12.9267368,17.0722105 L7.89473684,17.0723684 L7.89473684,13.7171053 L12.9267368,13.7162105 L12.9276316,8.68421053 L16.2828947,8.68421053 Z"
                                        id="Combined-Shape"
                                      ></path>
                                    </g>
                                  </g>
                                </g>
                              </svg>
                            </span>
                          </button>
                        </div>
                      </div>
                      <div className="right flex-grow-0 flex-1/3 flex items-end">
                        <span className="cart__item-variant-price">
                          {Spotview.formatMoney(item.final_line_price)}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className="cart__item-delete"
                  onClick={(e) => updateQuantity(e, item.variant_id, "delete")}
                >
                  <img src={binIcon} alt="" className="no-radius" />
                </div>
              </div>
            ))}
        </div>
      </div>
      <div className="ajax-cart-drawer__buttons">
        <div className="row cart__footer-final-total">
          <div className="ajax-cart__subtotal flex">
            <div className="ajax-cart__subtotal-label flex-1/2 ">
              {subtotal}:{" "}
            </div>
            <div className="ajax-cart__subtotal-price flex-1/2">
              {Spotview.formatMoney(cart.items_subtotal_price)}
            </div>
          </div>
          {/* <div className="col">
              <div><small style={{ color: 'gray' }}>(di cui Parafarmaco)</small></div> <div style={{ color: 'gray' }} id="kdo--parafarmaco">{calcTotaleParafarmaco(cartItems)}</div>
            </div> */}
          {cart.cart_level_discount_applications &&
            cart.cart_level_discount_applications.length > 0 &&
            cart.cart_level_discount_applications.map((discount, i) => (
              <div className="col">
                <div>
                  <small style={{ color: "gray" }}>Sconto </small>
                  {discount.title}
                </div>
                <div>
                  <small style={{ color: "gray" }} id="cartDiscount">
                    -{Spotview.formatMoney(discount.total_allocated_amount)}
                  </small>
                </div>
              </div>
            ))}
          {/* <div className="col">
              <div>Totale: </div>
              <div><strong id="kdo--total">{Spotview.formatMoney(cart.total_price)}</strong></div>
            </div> */}
          <div className="col">
            <small>{shippingCost}</small>
          </div>
        </div>
        {/* <form action="/cart?step=contact_information" method="post" noValidate className="cart">
            <button className="button button--primary" type="submit" name="checkout"  style={{ maxWidth: 'unset' }}>
              {customer ? <Translation value="cart.general.checkout" /> : <Translation value="cart.general.checkout_not_logged" />}
            </button>
          </form> */}
        <form
          action={window.routes.cart_url}
          className="cart__contents critical-hidden"
          method="post"
          id="cart"
        >
          <button
            type="submit"
            id="checkout"
            className="cart__checkout-button button button--primary mt-12 w-full"
            name="checkout"
            form="cart"
          >
            {checkout}
          </button>
        </form>
        <a
          href="/cart"
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            textDecoration: "none",
            padding: "20px 0",
            color: "gray",
          }}
        >
          {goToCart}
        </a>
      </div>
    </div>
  );
}
