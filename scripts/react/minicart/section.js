import React from 'react'
import { render } from 'react-dom'

import { getWindowVariable } from 'scripts/react/helpers'
import Minicart from './components/Minicart'

getWindowVariable('CART_DATA', (cartData) => {
  let customer = null
  if (window.CUSTOMER) {
    customer = window.CUSTOMER
  }
  render(<Minicart customer={customer} data={cartData} translations={window.THEME_CONFIGS.translations} themeSettings={window.THEME_CONFIGS.settings} minicartSettings={window.MINICART_SETTINGS} />, document.getElementById('ajax-cart'))
})