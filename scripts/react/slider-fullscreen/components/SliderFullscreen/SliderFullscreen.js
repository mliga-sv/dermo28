import React from 'react'

import { ButtonBack, ButtonNext, CarouselProvider, DotGroup, Slider, Slide } from 'pure-react-carousel'

import { useImageSrc } from 'scripts/react/hooks/use-image-src'
import { useImageSrcSet } from 'scripts/react/hooks/use-image-src-set'

import { useWindowWidth } from '@react-hook/window-size'

import store from 'scripts/state'

import classNames from 'classnames'

const SliderFullscreen = ({ slides, id }) => {
  const windowWidth = useWindowWidth()

  const isHeaderShrinked = store(state => state.header)
  
  const carouselClasses = classNames('slider-fullscreen', isHeaderShrinked ? 'shrinked' : '')


  const { settings: { breakpoints } } = window.THEME_CONFIGS

  const isMobile = breakpoints.mde && windowWidth < breakpoints.mde

  const renderImageSrc = (i) => {
    const { image } = slides[i]

    return { 
      imageSrc: useImageSrc(image, 720), 
      imageSrcSet: useImageSrcSet(image)
    }
  }

  const renderMobileImageSrc = (i) => {
    const { mobileImage } = slides[i]

    return { 
      imageSrc: useImageSrc(mobileImage, 720), 
      imageSrcSet: useImageSrcSet(mobileImage)
    }
  }

  return (
    <CarouselProvider
      className={carouselClasses}
      naturalSlideWidth={100}
      naturalSlideHeight={100}
      totalSlides={slides.length}
      dragEnabled={true}
      isIntrinsicHeight={true}
      infinite
      isPlaying
      interval={5000}
    >
      <div className="slider-fullscreen__controls">
        <DotGroup />
      </div>
      <Slider aria-label="Fullscreen slider" trayTag="div">
        {slides.map((slide, i) => (
          <Slide key={i} index={i} tag="div">
            <div>
              <div className="slider-fullscreen__image">
                {!isMobile && <img 
                  alt={slide.title} 
                  sizes="(max-width: 2024px) 50vw, 2048px"
                  srcSet={renderImageSrc(i).imageSrcSet} 
                  src={renderImageSrc(i).imageSrc}
                  className="no-radius w-full"
                />}
                {isMobile && <img 
                  alt={slide.title} 
                  sizes="(max-width: 540px) 540px, (max-width: 720px) 720px, (max-width: 900px) 900px, 1080px"
                  srcSet={renderMobileImageSrc(i).imageSrcSet} 
                  src={renderMobileImageSrc(i).imageSrc}
                  className="no-radius w-full"
                />}
              </div>
              <div className="slider-fullscreen__content">
                <div className="slider-fullscreen__info">
                  <div className="large-container">
                    <div className={"lg:w-1/2 " + (!isMobile ? (slide.textColor === "negative" ? "positive-text" : "negative-text") : (slide.textColorMobile === "negative" ? "positive-text" : "negative-text"))}>
                      <h2>{slide.title}</h2>
                      <p>{slide.description}</p>
                    </div>
                    {slide.ctaUrl && slide.ctaLabel && <a href={slide.ctaUrl} className="button button--primary button--medium mt-10">{slide.ctaLabel}</a>}
                  </div>
                </div>
              </div>
            </div>
          </Slide>
        ))}
      </Slider>
    </CarouselProvider>
  )
}

export default SliderFullscreen