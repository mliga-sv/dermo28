import React from 'react'

import { getWindowVariable, safeRender } from '../helpers'
import SliderFullscreen from './components/SliderFullscreen'

getWindowVariable('SLIDER_FULLSCREEN', (slider) => {
  const { slides, id, elementId } = slider

  if (slides && slides.length) {
    safeRender(<SliderFullscreen id={id} slides={slides} />, document.getElementById(elementId))
  }
})