import create from 'zustand'
import { subscribeWithSelector } from 'zustand/middleware'

// const store = create(subscribeWithSelector((set) => ({
//   menu: false,
//   header: false,
//   cart: {},
//   toggle: () => set(state => ({ menu: !state.menu })),
//   shrink: (payload) => set(state => ({ header: payload })),
//   setCart: (payload) => set(state => ({ cart: { ...state.cart, ...payload} }))
// })))

// export const { getState, setState, subscribe, destroy } = store

const store = create(subscribeWithSelector(set => ({
  menu: false,
  header: false,
  minicart: false,
  cart: {},
  cartChanged: false,
  cartLength: 0,
  availableProducts: {},
  toggle: () => set(state => ({ menu: !state.menu })),
  shrink: (payload) => set(state => ({ header: payload })),
  toggleMinicart: () => set(state => ({ minicart: !state.minicart })),
  changeCart: () => set(state => ({ cartChanged: !state.cartChanged })),
  setCart: (payload) => set(state => ({ cart: { ...state.cart, ...payload} })),
  setAvailableProducts: (payload) => set(state => ({ availableProducts: { ...state.availableProducts, ...payload} })),
  setCartLength: (payload) => set(state => ({ cartLength: payload }))
})))

export const { getState, setState, subscribe } = store
export default store