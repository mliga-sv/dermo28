import React from 'react'

import { subscribe, getState, setState } from 'scripts/state.js'

import { getWindowVariable, safeRender } from 'scripts/react/helpers'
import Sharer from './react/product/Sharer'

if (document.readyState !== 'loading') {
  onInit()
}
else {
  document.addEventListener('DOMContentLoaded', onInit)
}

function onInit() {
  const setCart = getState().setCart
  const changeCart = getState().changeCart
  const setCartLength = getState().setCartLength
  const toggleMinicart = getState().toggleMinicart
  const setAvailableProducts = getState().setAvailableProducts
  var minicart = getState().minicart

  subscribe(state => state.cartChanged, (state) => {
    let cartObject;
    
    fetch('/cart', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
    .then(res => res.json())
    .then(res => {
      cartObject = res
      
      const enrichItems = res.items.map(i => fetch(`/products/${i.handle}?view=full`, {
        method: 'GET',
        // headers: {
        //   Accept: 'application/json',
        //   'Content-Type': 'application/json',
        // }
      }))
      
      return Promise.all(enrichItems)
    })
    .then(res => Promise.all(res.map(r => r.text())))
    .then(res => {
      res.map(r => {
        r = JSON.parse(r.replaceAll('\n', ''))
        const cartItem = cartObject.items.find(o => o.id === r.product.variants[0].id)
        if (cartItem) {
          cartItem.product = { ...r.product, inventory: r.inventory }
        }
      })

      cartObject.items.map(i => {
        setAvailableProducts({
          [i.variant_id]: i.quantity < i.product.inventory
        });
      })
      
      window.CART_DATA = cartObject
      setCart(cartObject)
      setCartLength(cartObject.item_count) 
    })
    .catch(err => console.log(err))
  })
  
  // subscribe(state => state.cart, (state) => { console.log(state) })

  subscribe(state => state.availableProducts, (state) => window.AVAILABLE_PRODUCTS = state)
  
  setCart(window.CART_DATA)
  changeCart()
  
  subscribe(state => state.minicart, (state) => {
    minicart = state
    if (state) { 
      document.querySelector("body").style.overflow = 'hidden'
      document.querySelector(".kdo--ajax").classList.add('is-open')
    }
    else {
      document.querySelector("body").style.overflow = 'auto'
      document.querySelector(".kdo--ajax").classList.remove('is-open')
    }
  })

  var bp = window.THEME_CONFIGS && window.THEME_CONFIGS.settings && window.THEME_CONFIGS.settings.breakpoints && window.THEME_CONFIGS.settings.breakpoints.mde
  var isMobile = bp && window.innerWidth < bp
  
  window.addToCart = function (quantity, id) {
    return fetch('/cart/add.js', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        items: [
          {
            quantity,
            id: id
          }
        ]
      })
    })
    .then(res => {
      changeCart()
      if (!minicart && !isMobile) {
        toggleMinicart()
      }
      else if (isMobile) {
        const addToCartNotify = document.querySelector('#addToCartNotify')
        addToCartNotify.classList.add('opened')
        setTimeout(() => {
          addToCartNotify.classList.remove('opened')
        }, 1500)
      }

      if (window.location.pathname.includes('cart')) {
        window.location.reload()
      }
    })
    .catch(err => console.log(err))
  }

  getWindowVariable('BLOG_POST', (post) => {
    safeRender(<Sharer url={post.url} settings={post.socialSharing} />, document.getElementById('blogSharer'))
  })
}
