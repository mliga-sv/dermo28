const path = require('path')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')
const postCssPresetEnv = require('postcss-preset-env')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const tailwindcss = require('tailwindcss')
const postcssImport = require('postcss-import')
const exec = require('child_process').exec;

const isProduction = process.env.NODE_ENV === 'production'

// Generates script and link tags for asset files using Liquid syntax to get the correct shopify asset URL
const liquidTags = {
  js: (src) => `<script defer="defer" src="{{ '${src.slice(1)}' | asset_url }}"></script>`,
  css: (src) => `<link rel="stylesheet" href="{{ '${src.slice(1)}' | asset_url }}">`,
}

// Generates a config object for HtmlWebpackPlugin based on what type of assets are wanted (`js` or `css`)
const extractTags = (type, name, excludeName, ts) => ({
  filename: path.resolve(__dirname, `snippets/webpack-tags-${name}-${type}.liquid`),
  templateContent: ({ htmlWebpackPlugin: { files } }) => files[type].map(liquidTags[type]).join(''),
  inject: false,
  excludeChunks: [excludeName],
})

const babelConfig = {
  loader: 'babel-loader',
  options: {
      presets: [
          [
              'modern-browsers',
              {
                  loose: true,
                  modules: false,
              },
          ],
          [
              '@babel/preset-react',
              {
                  useBuiltIns: true,
              },
          ],
      ],
  },
}

const getFilename = (ext) => ({ runtime, chunk }) => {
  const nameParts = [
      // This will contain "vendors" to help identify 3rd-party code
      ...(chunk.idNameHints?.values?.() || []),
      // Runtime is either a string or a set of strings based on how many there are
      typeof runtime === 'string' && runtime,
      ...(runtime?.values?.() || []),
      // This is the entry name
      chunk.name,
  ]

  // Filter out blank name parts, ensure they are all unique, and join them up with a dash
  const name = [...new Set(nameParts.filter((namePart) => !!namePart))].join('-')

  // For production, use the name and the hash for caching and busting, for development use only the name
  // so that theme kit does not have to delete and upload a new file every time there is a change, and only updates
  // existing files
  return isProduction ? `compiled.${name}.[contenthash].${ext}` : `compiled.${name}.${ext}`
}

// Generates the webpack config object with slight differences between development and production modes
module.exports = {
  mode: isProduction ? 'production' : 'development',
  cache: false,
  entry: {
      theme: path.resolve(__dirname, 'scripts/theme.js'),
      // checkout: path.resolve(__dirname, 'scripts/checkout.js'),
  },
  output: {
      filename: getFilename('js'),
      path: path.resolve(__dirname, 'assets'),
      publicPath: '/',
  },
  module: {
      rules: [
          // {
          //     test: /\.module\.scss$/,
          //     use: [
          //         MiniCssExtractPlugin.loader,
          //         {
          //             loader: 'css-loader',
          //             options: {
          //                 modules: {
          //                     localIdentName: isProduction
          //                         ? '[sha1:hash:hex:6]'
          //                         : '[name]__[local]__[sha1:hash:hex:6]',
          //                     localIdentHashPrefix: isProduction ? `${new Date().getTime()}` : undefined,
          //                 },
          //             },
          //         },
          //         {
          //             loader: 'postcss-loader',
          //             options: {
          //               postcssOptions: {
          //                   plugins: [autoprefixer, cssnano, postCssPresetEnv],
          //               },
          //             }
          //         },
          //         {
          //             loader: 'sass-loader',
          //             options: {
          //                 additionalData: '@import "/styles/base";',
          //             },
          //         },
          //     ],
          // },
          {
              test: /\.scss$/,
              exclude: /\.module\.scss$/,
              use: [
                  MiniCssExtractPlugin.loader,
                  'css-loader',
                  {
                      loader: 'postcss-loader',
                      options: {
                        postcssOptions: {
                          plugins: [postcssImport, tailwindcss, autoprefixer, ...process.env.NODE_ENV === 'production' ? [cssnano] : [], postCssPresetEnv({
                            features: {
                              'focus-within-pseudo-class': false
                            }
                          })],
                        }
                      },
                  },
                  'sass-loader',
              ],
          },
          {
              test: /(react-async|camelcase-keys|map-obj).+\.js$/,
              use: babelConfig,
          },
          {
              test: /\.js$/,
              exclude: /node_modules/,
              use: babelConfig,
          },
          {
              test: /\.svg$/,
              exclude: /node_modules/,
              use: {
                  loader: 'svg-react-loader',
              },
          },
          {
              test: /\.(jpe?g|png|gif)$/i,
              exclude: /node_modules/,
              use: [
                  {
                      loader: 'url-loader',
                      // options: {
                      //     limit: 10000,
                      //     name: '[name].[ext]',
                      // },
                  }
              ],
          },
      ],
  },
  resolve: {
      alias: {
          scripts: path.resolve(__dirname, 'scripts/'),
          styles: path.resolve(__dirname, 'styles/'),
          assets: path.resolve(__dirname, 'assets'),
      },
  },
  plugins: [
      new CleanWebpackPlugin({
          cleanOnceBeforeBuildPatterns: ['compiled*', path.resolve(__dirname, 'snippets/webpack-tags*')],
      }),
      new MiniCssExtractPlugin({
          filename: getFilename('css'),
      }),
      // Theme CSS
      new HtmlWebpackPlugin(extractTags('css', 'theme', 'checkout')),
      // Theme JS
      new HtmlWebpackPlugin(extractTags('js', 'theme', 'checkout')),
      // Checkout CSS
      new HtmlWebpackPlugin(extractTags('css', 'checkout', 'theme')),
      // Checkout JS
      new HtmlWebpackPlugin(extractTags('js', 'checkout', 'theme')),
      {
        apply: (compiler) => {
          compiler.hooks.afterEmit.tap('AfterEmitPlugin', (compilation) => {
            exec('echo " " >> /Users/gabrieleciolino/shopifyProjects/fattoriegarofalo/snippets/dummy.liquid', (err, stdout, stderr) => {
              if (stdout) process.stdout.write(stdout);
              if (stderr) process.stderr.write(stderr);
              console.log('write success')
            });
          });
        }
      }
  ],
  optimization: {
      minimize: isProduction,
      minimizer: [new TerserPlugin()],
      splitChunks: {
          chunks: 'all',
          automaticNameDelimiter: '-',
      },
  },
  performance: {
      hints: false,
  },
}