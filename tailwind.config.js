module.exports = {
  mode: 'jit',
  purge: [
    "./scripts/**/*.{js,jsx,ts,tsx,vue}",
    // "./snippets/**/*.liquid",
    "./sections/*.liquid",
    "./templates/**/*",
    "./layout/*.liquid",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      black: '#231F20',
      yellow: '#FFD100',
      lightGrey: '#E8E8E8',
      darkGrey: '#747476',
      red: '#A12A2F',
      white: '#ffffff'
    },
    fontSize: {
      'xs': '14px',
      'sm': '16px',
      'md': '18px'
    },
    screens: {
      'sm': '640px',
      'md': '768px',
      'mde': '769px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
    },
    extend: {
      maxWidth: {
        '1/3': '33%',
				'31px': '31px'
      },
      flex: {
        '30%': '1 0 30%',
        '70%': '1 0 70%',
        '40%': '1 0 40%',
        '60%': '1 0 60%',
        '1/5': '0 0 calc(100%/5)',
        '1/4': '1 0 calc(100%/4)',
        '1/3': '1 0 calc(100%/3)',
        '1/2': '1 0 calc(100%/2)',
        '100%': '1 0 100%'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
